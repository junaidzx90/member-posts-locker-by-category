jQuery(function( $ ) {
	'use strict';

	$('#membertags, #members_categories').select2({
		placeholder: "Select a tag"
	});

	$('#expirydateinp').datepicker({ dateFormat: 'yy-mm-dd' });
});