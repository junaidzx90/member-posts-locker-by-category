<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Member_Posts
 * @subpackage Member_Posts/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Member_Posts
 * @subpackage Member_Posts/admin
 * @author     Junayed <admin@easeare.com>
 */
class Member_Posts_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Member_Posts_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Member_Posts_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;
		if(get_post_type( $post ) === 'members' || (isset($_GET['post_type']) && $_GET['post_type'] === 'members')){
			wp_enqueue_style( 'jquery-ui', 'https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css', array(), $this->version, 'all' );
			wp_enqueue_style( 'select2cdn', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css', array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/member-posts-admin.css', array(), microtime(), 'all' );
		}

	}

	function admin_footer_script(){
		global $post;

		if(isset($_GET['post_type']) && $_GET['post_type'] === 'members' && get_current_screen(  )->base === 'edit'){
			?>
			<script>
				jQuery('.page-title-action').after('<form method="post" enctype="multipart/form-data" class="csvbtns"><label class="button-secondary memimport">Import <input type="file" name="membercsv" accept=".csv" id="mpcsv"> </label><a href="?post_type=members&action=export" class="button-secondary memexport">Export</a></form>');

				jQuery('#mpcsv').on("change", function(){
					jQuery('.csvbtns').trigger("submit");
				});
			</script>
			<?php
		}
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Member_Posts_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Member_Posts_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;
		if(get_post_type( $post ) === 'members' || (isset($_GET['post_type']) && $_GET['post_type'] === 'members')){
			wp_enqueue_script( 'jquery-ui', 'https://code.jquery.com/ui/1.10.4/jquery-ui.js', array( ), $this->version, false );
			wp_enqueue_script( 'select2cdn', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array( ), $this->version, false );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/member-posts-admin.js', array( 'jquery','jquery-ui' ), $this->version, false );
		}
	}


	function members_list(){
		$labels = array(
			'name'                  => __( 'Members', 'member-posts' ),
			'singular_name'         => __( 'members', 'member-posts' ),
			'menu_name'             => __( 'Members', 'member-posts' ),
			'name_admin_bar'        => __( 'members', 'member-posts' ),
			'add_new'               => __( 'New member', 'member-posts' ),
			'add_new_item'          => __( 'New member', 'member-posts' ),
			'new_item'              => __( 'New member', 'member-posts' ),
			'edit_item'             => __( 'Edit member', 'member-posts' ),
			'view_item'             => __( 'View member', 'member-posts' ),
			'all_items'             => __( 'All members', 'member-posts' ),
			'search_items'          => __( 'Search members', 'member-posts' ),
			'parent_item_colon'     => __( 'Parent members:', 'member-posts' ),
			'not_found'             => __( 'No members found.', 'member-posts' ),
			'not_found_in_trash'    => __( 'No members found in Trash.', 'member-posts' )
		);     
		$args = array(
			'labels'             => $labels,
			'description'        => 'members custom post type.',
			'public'             => true,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'members' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => 45,
			'menu_icon'      	 => 'dashicons-admin-users',
			'supports'           => array( 'title' ),
			'show_in_rest'       => false
		);
		  
		register_post_type( 'members', $args );
	}
	
	// Overrides predefined texts
	function eser_translation_mangler($translation, $text, $domain) {
        global $post;
		if (get_post_type( $post ) == 'members') {
			if ( $text == 'Publish' )
				return 'Save Member';
			if ( $text == 'Update' )
				return 'Update Member';
			if ( $text == 'Post updated.' )
				return 'Member Updated.';
			if ( $text == 'Post published.' )
				return 'Member Added.';
			if ( $text == 'Add title' )
				return 'Member name';
			return $translation;
		}
	
		return $translation;
	}

	// Manage table columns
	function manage_members_columns($columns) {
		unset(
			$columns['subscribe-reloaded'],
			$columns['title'],
			$columns['date']
		);
	
		$new_columns = array(
			'title' => __('Name', 'member-posts'),
			'member_unq_id' => __('ID', 'member-posts'),
			'member_tag' => __('Tag', 'member-posts'),
			'expire' => __('Expire', 'member-posts'),
			'visits' => __('Visits', 'member-posts'),
			'lastvisits' => __('Last Visit', 'member-posts'),
			'date' => __('Joined', 'member-posts'),
		);
	
		return array_merge($columns, $new_columns);
	}

	// View custom column data
	function manage_members_columns_views($column_id, $post_id){
		switch ($column_id) {
			case 'member_unq_id':
				if(get_post_meta( $post_id, 'member_unique_id', true )){
					echo get_post_meta( $post_id, 'member_unique_id', true );
				}else{
					echo 'Invalid member!';
				}
				break;
			case 'member_tag':
				if(get_post_meta( $post_id, 'membertags', true )){
					echo implode(', ', get_post_meta( $post_id, 'membertags', true ));
				}else{
					echo '';
				}
				break;

			case 'expire':
				echo get_post_meta($post_id, 'expirydate', true);
				break;
			case 'visits':
				echo get_post_meta($post_id, 'numberofvisit', true);
				break;
			case 'lastvisits':
				echo get_post_meta($post_id, 'lastvisit', true);
				break;
			default:
				# code...
				break;
		}
	}
	
	// Manage sortable column
	function manage_members_columns_sortable($columns){
		$columns['member_unq_id'] = 'Member ID';
		return $columns;
	}

	// Remove Quick edit pmember posts
	function remove_quick_edit_members( $actions, $post ) {
		if(get_post_type( $post ) === 'members'){
			unset($actions['inline hide-if-no-js']);
			return $actions;
		}else{
			return $actions;
		}
   	}

	//   Remove edit option from bulk
	function remove_member_edit_actions( $actions ){
		unset( $actions['edit'] );
		return $actions;
   	}
	
	//    Add meta boxes
	function member_meta_boxes(){
		global $wp_meta_boxes;
		unset($wp_meta_boxes['members']);

		add_meta_box( 'submitdiv', 'Save Member', 'post_submit_meta_box', 'members', 'side' );
		add_meta_box( 'expirydate', 'Expiry date', [$this, 'expiry_date_callback'], 'members', 'side' );
		add_meta_box( 'lastvisit', 'Last Visit', [$this, 'lastvisit_callback'], 'members', 'side' );
		add_meta_box( 'numberofvisit', 'Number of visits', [$this, 'numberofvisit_callback'], 'members', 'side' );
		add_meta_box( 'member_unq_id', 'Member ID', [$this, 'member_unique_id'], 'members', 'advanced' );
		add_meta_box( 'member_tags', 'Tags', [$this, 'member_tags'], 'members', 'advanced' );
	}

	// Expiryy date
	function expiry_date_callback($post){
		echo '<input class="widefat" type="text" placeholder="yyyy-mm-dd" name="expirydate" value="'.get_post_meta($post->ID, 'expirydate', true).'" id="expirydateinp">';
	}

	// Last visit
	function lastvisit_callback($post){
		echo '<table>
			<tbody>
				<tr>
					<th>Last date: </th>
					<td>'.(get_post_meta($post->ID, 'lastvisit', true) ? get_post_meta($post->ID, 'lastvisit', true) : 'mm/dd/yyyy').'</td>
				</tr>
			</tbody>
		</table>';
	}

	// Last visit
	function numberofvisit_callback($post){
		echo '<table>
			<tbody>
				<tr>
					<th>Visit counts: </th>
					<td>'.(get_post_meta($post->ID, 'numberofvisit', true) ? get_post_meta($post->ID, 'numberofvisit', true) : 0).'</td>
				</tr>
			</tbody>
		</table>';
	}

	// Member id callback
	function member_unique_id($post){
		$unique = get_post_meta($post->ID, 'member_unique_id', true);
		
		echo '<input type="text" style="width: 50%;" placeholder="Member Unique ID" value="'.$unique.'" name="member_unq_id">';
	}

	// Member tags
	function member_tags($post){
		$terms = get_terms( array(
			'taxonomy' => 'post_tag',
			'hide_empty' => false,
		) );

		$saved = get_post_meta($post->ID, 'membertags', true);

		echo '<select class="widefat" multiple name="membertags[]" id="membertags">';
		if($terms){
			if(sizeof($terms)){
				if(!is_array($saved)){
					$saved = [];
				}
				foreach($terms as $term){
					if(in_array($term->slug, $saved)){
						echo "<option selected value='$term->slug'>$term->name</option>";
					}else{
						echo "<option value='$term->slug'>$term->name</option>";
					}
				}
			}
		}
		echo '</select>';
	}

	// Save members data
	function members_post_save($post_id){
		global $wpdb;
		if(isset($_POST['member_unq_id'])){
			$unique_id = sanitize_text_field( $_POST['member_unq_id'] );
			update_post_meta($post_id, 'member_unique_id', $unique_id);
		}

		if(isset($_POST['expirydate'])){
			update_post_meta( $post_id, 'expirydate', $_POST['expirydate'] );
		}

		if(isset($_POST['membertags'])){
			$membertags = $_POST['membertags'];
			update_post_meta($post_id, 'membertags', $membertags);
		}else{
			update_post_meta($post_id, 'membertags', []);
		}
	}

	// Posts option
	function members_posts_settings(){
		add_submenu_page( 'edit.php?post_type=members', 'Settings', 'Settings', 'manage_options', 'setting', [$this, 'mv_post_settings'], null );
		// options
		add_settings_section( 'mvoption_settings_section', '', '', 'mvoption_settings_page' );

		// Category
		add_settings_field( 'members_categories', 'Categories', [$this, 'members_categories_cb'], 'mvoption_settings_page', 'mvoption_settings_section');
		register_setting( 'mvoption_settings_section', 'members_categories');
		// Error page
		add_settings_field( 'members_error_page', 'Error page', [$this, 'members_error_page_cb'], 'mvoption_settings_page', 'mvoption_settings_section');
		register_setting( 'mvoption_settings_section', 'members_error_page');
		// Error page
		add_settings_field( 'members_error_page2', 'Error page (Expiry date)', [$this, 'members_error_page2_cb'], 'mvoption_settings_page', 'mvoption_settings_section');
		register_setting( 'mvoption_settings_section', 'members_error_page2');
	}

	// Categories
	function members_categories_cb(){
		$categories = get_categories( ['hide_empty' => false] );
		if($categories){
			echo '<select class="widefat" multiple name="members_categories[]" id="members_categories">';
			$selected = get_option( 'members_categories' );
			if(!is_array($selected)){
				$selected = [];
			}
			foreach($categories as $category){
				if(in_array($category->term_id, $selected)){
					echo '<option selected value="'.$category->term_id.'">'.$category->name.'</option>';
				}else{
					echo '<option value="'.$category->term_id.'">'.$category->name.'</option>';
				}
			}
			echo '</select>';
		}
	}

	// Error page callback
	function members_error_page_cb(){
		$dropdown_args = array(
			'post_type'        => 'page',
			'selected'         => get_option('members_error_page'),
			'name'             => 'members_error_page',
			'show_option_none' => 'Select',
			'echo'             => 0,
		);
		
		echo wp_dropdown_pages( $dropdown_args );
	}
	// Error page callback
	function members_error_page2_cb(){
		$dropdown_args = array(
			'post_type'        => 'page',
			'selected'         => get_option('members_error_page2'),
			'name'             => 'members_error_page2',
			'show_option_none' => 'Select',
			'echo'             => 0,
		);
		
		echo wp_dropdown_pages( $dropdown_args );
	}

	// Option callback
	function mv_post_settings(){
		echo '<h3>Settings</h3><hr>';
		echo '<div class="mvoption_content">';
		echo '<form style="width: 50%" method="post" action="options.php">';
		echo '<table class="widefat">';
		settings_fields( 'mvoption_settings_section' );
		do_settings_fields( 'mvoption_settings_page', 'mvoption_settings_section' );
		echo '</table>';
		submit_button();
		echo '</form>';
		echo '</div>';
	}

	// Remove unxpected metadata
	function remove_unknownmetadata(){
		global $wpdb;
		$ids =  $wpdb->get_results("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = 'member_unique_id'");
		if($ids){
			foreach($ids as $id){
				$id = $id->post_id;
				if(!$wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE ID = $id AND post_type = 'members' AND post_status = 'publish'")){
					delete_post_meta($id, 'member_unique_id');
				}
			}
		}
	}

	// Post view counter
	function post_view_counter($post_id){
		$member_id = null;
		if(isset($_GET['id'])){
			$member_id = $_GET['id'];
		}else{
			return;
		}
		$visits = get_post_meta( $post_id, 'mp_visits', true );
		if(!is_array($visits)){
			$visits = array();
		}

		if(!array_key_exists($member_id, $visits)){
			$count = get_post_meta($post_id, 'numberofvisit', true );
			$count = intval($count);
			$counter = $count += 1;
			update_post_meta( $post_id, 'numberofvisit', $counter );
		}

		$visits[$member_id] = date('y-m-d');
		update_post_meta( $post_id, 'mp_visits', $visits );

		$visits = get_post_meta( $post_id, 'mp_visits', true );

		if(is_array($visits)){
			foreach($visits as $key => $visit){
				if(strtotime($visit) < strtotime(date('y-m-d'))){
					unset($visits[$key]);
				}
			}
		}

		update_post_meta( $post_id, 'mp_visits', $visits );
	}

	// The content
	function members_the_content($contents){
		global $post, $wpdb;
		
		if(!is_admin(  )){
			if(is_singular( 'post' )){
				$rediect = '';
				if(get_option('members_error_page')){
					$rediect = esc_url( get_the_permalink( get_option('members_error_page') ) );
				}

				$rediect2 = '';
				if(get_option('members_error_page2')){
					$rediect2 = esc_url( get_the_permalink( get_option('members_error_page2') ) );
				}

				$selectedCats = get_option( 'members_categories' ); //plugin categories
				
				if(isset($_GET['id'])){ 
					$reqId = sanitize_text_field( $_GET['id'] );

					setcookie(
						"mp_postid",
						$reqId,
						time() + (10 * 365 * 24 * 60 * 60),
						'/'
					);
					
					$member_id = $wpdb->get_var("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = 'member_unique_id' AND meta_value = '$reqId'");

					$today = date('y-m-d');
					$expiry = get_post_meta($member_id, 'expirydate', true);

					
		
					if($member_id){ // Not allowed wrong id
						$post_cat = get_the_category($post->ID);
						$post_cats = [];
						if($post_cat){
							foreach($post_cat as $pcat){
								$post_cats[] = $pcat->term_id;
							}

							if($selectedCats && array_intersect($post_cats, $selectedCats)){

								if(!empty($expiry)){
									if(strtotime($today) > strtotime($expiry)){ //Expired
										// Redirect2
										if($rediect2){
											if($post->ID !== intval( get_option('members_error_page2') )){
												wp_safe_redirect( $rediect2 );
												exit;
											}
										}
									}
								}

								$memberTags = get_post_meta($member_id, 'membertags', true);
								if(is_array($memberTags) && !empty($memberTags[0])){
									$tgs = get_the_tags($post->ID);
									$postTags = [];
									if(is_array($tgs)){
										foreach($tgs as $tg){
											$postTags[] = $tg->slug;
										}
									}
									
									if(empty(array_intersect($postTags, $memberTags))){
										// Redirect
										if($rediect){
											if($post->ID !== intval( get_option('members_error_page') ))
												wp_safe_redirect( $rediect );
										}
									}
								}
							}

							// Counter & lastvisit
							if(strtotime($today) <= strtotime($expiry)){
								$this->post_view_counter($member_id);
								update_post_meta( $member_id, 'lastvisit', $today );
							}else{
								$this->post_view_counter($member_id);
								update_post_meta( $member_id, 'lastvisit', $today );
							}
						}

					}else{
						$post_cat = get_the_category($post->ID);
						$post_cats = [];
						if($post_cat){
							foreach($post_cat as $pcat){
								$post_cats[] = $pcat->term_id;
							}
							
							if($selectedCats && array_intersect($post_cats, $selectedCats)){

								if(!empty($expiry)){
									if(strtotime($today) > strtotime($expiry)){ //Expired
										// Redirect2
										if($rediect2){
											if($post->ID !== intval( get_option('members_error_page2') )){
												wp_safe_redirect( $rediect2 );
												exit;
											}
										}
									}
								}

								if($rediect){
									if($post->ID !== intval( get_option('members_error_page') ))
										wp_safe_redirect( $rediect );
								}
							}

							// Counter & lastvisit
							if(strtotime($today) <= strtotime($expiry)){
								$this->post_view_counter($member_id);
								update_post_meta( $member_id, 'lastvisit', $today );
							}else{
								$this->post_view_counter($member_id);
								update_post_meta( $member_id, 'lastvisit', $today );
							}
						}
					}

				}else{
					if(isset($_COOKIE['mp_postid'])){
						wp_safe_redirect( get_the_permalink( $post ).'?id='.$_COOKIE['mp_postid'] );
					}else{
						$post_cat = get_the_category($post->ID);
						$post_cats = [];
						if($post_cat){
							foreach($post_cat as $pcat){
								$post_cats[] = $pcat->term_id;
							}
							
							if($selectedCats && array_intersect($post_cats, $selectedCats)){
								if($rediect){
									if($post->ID !== intval( get_option('members_error_page') ))
										wp_safe_redirect( $rediect );
								}
							}
							
						}
					}
				}
			}
		}
		

		return $contents;
	}

	// Export & Import
	function export_import_callback(){
		if(isset($_GET['action']) && $_GET['action'] === 'export'){
			$this->exportmembers();
		}

		if(isset($_FILES['membercsv'])){
			$this->importmembers();
		}
	}

	// Export csv file
	function exportmembers(){
		$export_fields = array(
			'name',
			'id',
			'tags',
			'expire',
			'visits',
			'lastvisits'
		);
		// Output file stream
		$output_filename = 'Members_' . strftime( '%Y-%m-%d' )  . '.csv';
		$output_handle = @fopen( 'php://output', 'w' );

		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( 'Content-Disposition: attachment; filename=' . $output_filename );
		header( 'Expires: 0' );
		header( 'Pragma: public' );

		$args = array(
			'post_type' => 'members',
			'post_status'	=> 'publish',
			'posts_per_page' => -1,
			'orderby'		 => 'date',
			'order'          => 'ASC'
		);

		$members = get_posts($args);
		
		$firstRow = [];
		// Build export array
		foreach ( $export_fields as $export_field ) {
			$firstRow[] = $export_field;
		}
		// Add row to file
		fputcsv( $output_handle, $firstRow );

		if($members){
			foreach($members as $member){
				$list = [];
				$member_id = get_post_meta($member->ID, 'member_unique_id', true);
				$tags = get_post_meta($member->ID, 'membertags', true);
				$tags = implode(',', $tags);
				$expire = get_post_meta($member->ID, 'expirydate', true);
				$visits = get_post_meta($member->ID, 'numberofvisit', true);
				$lastvisits = get_post_meta($member->ID, 'lastvisit', true);

				$list[] = $member->post_title;
				$list[] = $member_id;
				$list[] = $tags;
				$list[] = $expire;
				$list[] = $visits;
				$list[] = $lastvisits;

				// Add row to file
				fputcsv( $output_handle, $list );
			}
		}

		// Close output file stream
		fclose( $output_handle );

		// We're done!
		exit;
	}

	function get_term_id($slug){
		$idObj = get_category_by_slug( $slug );
 
		if ( $idObj instanceof WP_Term ) {
			$id = $idObj->term_id;

			return $id;
		}
	}

	// Import members
	function importmembers(){
		global $wpdb;
		$fileName = $_FILES["membercsv"]["tmp_name"];
    
    	if ($_FILES["membercsv"]["size"] > 0) {
        
			$file = fopen($fileName, "r");
			
			$first_row = true;
			$array = [];
			while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
				if($first_row){
					$first_row = false;
				}else{
					$single = array(
						'name' => $column[0],
						'id' => $column[1],
						'tags' => $column[2],
						'expire' => $column[3]
					);

					$array[] = $single;
				}
			}
			fclose( $file );

			if(sizeof($array) > 0){
				foreach($array as $member){
					$mp_id = $member['id'];

					$p_id = $wpdb->get_var("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_value = '$mp_id' AND meta_key = 'member_unique_id'");

					$post_id = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE ID = '$p_id' AND post_status = 'publish' AND post_type = 'members'");

					if(!$post_id){
						$post_id = wp_insert_post( array(
							'post_type' 	=> 'members',
							'post_status'   => 'publish',
							'post_title'  	=> $member['name'],
							'post_content'  => '',
							'post_author'   => get_current_user_id(  )
						) );
					}else{
						wp_update_post( array(
							'ID'			=> $post_id,
							'post_type' 	=> 'members'
						) );
					}

					if($post_id){
						update_post_meta($post_id, 'member_unique_id', $member['id']);
						update_post_meta($post_id, 'membertags', explode(',', $member['tags']));
						update_post_meta($post_id, 'expirydate', $member['expire']);
					}
				}
			}
			
		}
	}
}
