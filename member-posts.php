<?php
ob_start();
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.fiverr.com/junaidzx90
 * @since             1.0.0
 * @package           Member_Posts
 *
 * @wordpress-plugin
 * Plugin Name:       Member Posts
 * Plugin URI:        https://www.fiverr.com
 * Description:       This plugin works for prevent post access by tags with unique id.
 * Version:           1.0.0
 * Author:            junaidzx90
 * Author URI:        https://www.fiverr.com/junaidzx90
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       member-posts
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'MEMBER_POSTS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-member-posts-activator.php
 */
function activate_member_posts() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-member-posts-activator.php';
	Member_Posts_Activator::activate();
}

// add_action( 'init', function(){
// 	$args = ['post_type' => 'members', 'post_status' => 'publish', 'numberposts' => '-1'];
// 	$posts = get_posts($args);
// 	if($posts){
// 		foreach($posts as $post){
// 			delete_post_meta( $post->ID, 'numberofvisit' );
// 			delete_post_meta( $post->ID, 'mp_visits' );
// 			delete_post_meta( $post->ID, 'lastvisit' );
// 		}
// 	}
// } );

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-member-posts-deactivator.php
 */
function deactivate_member_posts() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-member-posts-deactivator.php';
	Member_Posts_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_member_posts' );
register_deactivation_hook( __FILE__, 'deactivate_member_posts' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-member-posts.php';
date_default_timezone_set(get_option('timezone_string')?get_option('timezone_string'):'UTC');
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_member_posts() {

	$plugin = new Member_Posts();
	$plugin->run();

}
run_member_posts();
