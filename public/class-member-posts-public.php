<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Member_Posts
 * @subpackage Member_Posts/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Member_Posts
 * @subpackage Member_Posts/public
 * @author     Junayed <admin@easeare.com>
 */
class Member_Posts_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Member_Posts_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Member_Posts_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/member-posts-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Member_Posts_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Member_Posts_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;
		if(get_post_type( $post ) == 'post')
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/member-posts-public.js', array( 'jquery' ), $this->version, false );

	}

	// get_requestfrom_webhook
	function get_requestfrom_webhook(){
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			foreach($_POST as $contact){
				if(is_array($contact)){
					$first_name = null;
					if(array_key_exists('first_name', $contact)){
						$first_name = $contact['first_name'];
					}
					$tags = [];
					if(array_key_exists('tags', $contact)){
						$tgarr = $contact['tags'];
						$tgarr = explode(',', $tgarr);

						if($tgarr && is_array($tgarr)){
							foreach($tgarr as $tg){
								$tag = str_replace(" ", "", strtolower($tg));
								$tags[] = $tag;
							}
						}
					}
					$buid = null;
					$buexpire = null;
					if(array_key_exists('fields', $contact)){
						if(array_key_exists('buid', $contact['fields'])){
							$buid = $contact['fields']['buid'];
						}
						if(array_key_exists('buexpire', $contact['fields'])){
							$buexpire = $contact['fields']['buexpire'];
							$buexpire = explode('T', $buexpire)[0];
							$buexpire = date('y-m-d', strtotime("+1 day", strtotime($buexpire)));
						}
					}

					if($first_name && $tags && $buid && $buexpire){
						global $wpdb;

						$mpid = $wpdb->get_var("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_value = '$buid' AND meta_key = 'member_unique_id'");

						$post_id = null;
						if($mpid){
							$post_id = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE ID = $mpid AND post_status = 'publish' AND post_type = 'members'");
						}

						if(!$post_id){
							$post_id = wp_insert_post( array(
								'post_type' 	=> 'members',
								'post_status'   => 'publish',
								'post_title'  	=> $first_name,
								'post_content'  => '',
								'post_author'   => get_current_user_id(  )
							) );
						}else{
							wp_update_post( array(
								'ID'			=> $post_id,
								'post_type' 	=> 'members'
							) );
						}

						if($post_id){
							update_post_meta($post_id, 'member_unique_id', $buid);
							update_post_meta($post_id, 'membertags', $tags);
							update_post_meta($post_id, 'expirydate', $buexpire);
						}
					}
				}
			}
		}
	}
}
