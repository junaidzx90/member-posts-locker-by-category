<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.fiverr.com/junaidzx90
 * @since      1.0.0
 *
 * @package    Member_Posts
 * @subpackage Member_Posts/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
